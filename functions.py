from client import client;
import settings as sett;
import re;
attrName=[];
attributes=[];
c1=client();
def tab(quant=1):
  i=1
  tab="\t";
  for i in range(quant-1):
    tab+="\t";
  return tab;


def br(quant=1):
  br="\n"
  for i in range(quant-1):
    br+="\n";
  return br



def chkDate(regex):
  return bool(re.match("[0-9]{2}/[0-9]{2}/[0-9]{2}",regex));

def chkString(regex):
  return bool(re.match("[^a-zA-z0-9!@#$%^&*(),.?\":;{}|<>\[\]\\\/']",regex))

def convert(stdin,conv="str"):
  if(chkDate(stdin)==True):
    return str(stdin)
  elif(stdin.isdecimal() and conv=="float"):
    return float(stdin);
  elif(stdin.isdigit() and conv=="int"):
    return int(stdin);
  return stdin;

def creatinMenu():
  i=1;
  print(" ********************************* ");
  print("*    "+str(i)+"-cadastrar cliente");i+=1;
  print("*    "+str(i)+"-consultar cliente");i+=1;         print("*    "+str(i)+"-apagar dados");i+=1;            
  print(" ********************************* ");

#@serviceCalc
def serviceCalc(quant,price):
  return quant*price;


def message(obj):
  return "starting signup from "+obj.__class__.__name__;

def messageImplements():
  print("not yet build it!,text me a email");
  print("ceo:"+tab(2)+str(sett.ceo));
  print("email:"+tab(2)+str(sett.email));
  


def isEmpty(attr):
  while not attr:
    return True
  return False;

digits_letters="^A-Za-z+0-9";
special_simple_char="!@#$%^&*(),.?\":{}|<>'~çÇ;\[\]";
only_number="aA-zZ"+special_simple_char;
only_date='\d{2}/\d{2}/\d{2}';
def isSpecialChar(attr):
  #accent="[^A-Za-z+0-9]";
  return bool(re.match("["+digits_letters+"]",attr))

def isSpecialSimpleChar(attr):
  #accent="[!@#$%^&*(),.?\":{}|<>'~çÇ;\[\]]";
  return bool(re.match("["+special_simple_char+"]",attr))


def onlyNumber(attr):
  #accent="[aA-zZ!@#$%^&*(),.?\":{}|<>'~çÇ;]"
  return bool(re.match("["+only_number+"]",attr))


def isdate(attr):
  return  bool(re.match(only_date,attr))

def isEnterNos(attr):
  return  bool(re.match("[a-rt-zA-RT-Z0-9]",attr))

def isEnter(attr):
  return  bool(re.match("[^a-zA-Z0-9!@#$%^&*(),.?\":{}|<>'~çÇ;\[\]]",attr))

